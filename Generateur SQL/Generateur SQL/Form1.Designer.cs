﻿
namespace Generateur_SQL
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.importTable = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.convert = new System.Windows.Forms.Button();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.lbxChamps = new System.Windows.Forms.ListBox();
            this.lbxDossier = new System.Windows.Forms.ListBox();
            this.bntDossier = new System.Windows.Forms.Button();
            this.FDB = new System.Windows.Forms.FolderBrowserDialog();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.tbxSQL = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // importTable
            // 
            this.importTable.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.importTable.Location = new System.Drawing.Point(308, 75);
            this.importTable.Name = "importTable";
            this.importTable.Size = new System.Drawing.Size(124, 39);
            this.importTable.TabIndex = 0;
            this.importTable.Text = "Importer une table";
            this.importTable.UseVisualStyleBackColor = true;
            this.importTable.Click += new System.EventHandler(this.importTable_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(970, 269);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(120, 94);
            this.checkedListBox1.TabIndex = 1;
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // convert
            // 
            this.convert.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.convert.Location = new System.Drawing.Point(308, 399);
            this.convert.Name = "convert";
            this.convert.Size = new System.Drawing.Size(124, 39);
            this.convert.TabIndex = 3;
            this.convert.Text = "Sauvergarder table importer";
            this.convert.UseVisualStyleBackColor = true;
            this.convert.Click += new System.EventHandler(this.convert_Click);
            // 
            // lbxChamps
            // 
            this.lbxChamps.FormattingEnabled = true;
            this.lbxChamps.ItemHeight = 15;
            this.lbxChamps.Location = new System.Drawing.Point(258, 131);
            this.lbxChamps.Name = "lbxChamps";
            this.lbxChamps.Size = new System.Drawing.Size(202, 244);
            this.lbxChamps.TabIndex = 5;
            // 
            // lbxDossier
            // 
            this.lbxDossier.FormattingEnabled = true;
            this.lbxDossier.ItemHeight = 15;
            this.lbxDossier.Location = new System.Drawing.Point(50, 131);
            this.lbxDossier.Name = "lbxDossier";
            this.lbxDossier.Size = new System.Drawing.Size(151, 244);
            this.lbxDossier.TabIndex = 6;
            this.lbxDossier.SelectedIndexChanged += new System.EventHandler(this.lbxDossier_SelectedIndexChanged);
            // 
            // bntDossier
            // 
            this.bntDossier.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.bntDossier.Location = new System.Drawing.Point(64, 75);
            this.bntDossier.Name = "bntDossier";
            this.bntDossier.Size = new System.Drawing.Size(124, 39);
            this.bntDossier.TabIndex = 7;
            this.bntDossier.Text = "Dossier";
            this.bntDossier.UseVisualStyleBackColor = true;
            this.bntDossier.Click += new System.EventHandler(this.bntDossier_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // tbxSQL
            // 
            this.tbxSQL.Location = new System.Drawing.Point(478, 131);
            this.tbxSQL.Multiline = true;
            this.tbxSQL.Name = "tbxSQL";
            this.tbxSQL.Size = new System.Drawing.Size(312, 244);
            this.tbxSQL.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(170)))), ((int)(((byte)(200)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(801, 59);
            this.panel1.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Fax", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(316, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 24);
            this.label1.TabIndex = 11;
            this.label1.Text = "Générateur SQL";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bntDossier);
            this.Controls.Add(this.tbxSQL);
            this.Controls.Add(this.lbxDossier);
            this.Controls.Add(this.lbxChamps);
            this.Controls.Add(this.convert);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.importTable);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button importTable;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.Button convert;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.ListBox lbxChamps;
        private System.Windows.Forms.ListBox lbxDossier;
        private System.Windows.Forms.Button bntDossier;
        private System.Windows.Forms.FolderBrowserDialog FDB;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.TextBox tbxSQL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
    }
}

