﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.LinkLabel;

namespace Generateur_SQL
{
    public partial class Form1 : Form
    {
        String [] valuesTable;
        String [] insertTab;
        String nom;
        String name;
        public Form1()
        {
            InitializeComponent();
        }

        private void importTable_Click(object sender, EventArgs e)
        {
            String ligne;
            OFD.Title = "Les fichiers texte."; //titre de la boite de dialogue
            OFD.Filter = "Fichiers texte|*.txt";// Filtre : n'affiche que les fichiers ".txt"
            if (OFD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    lbxChamps.Items.Clear();//Efface le contenu de la listBox
                    StreamReader SR = new StreamReader(OFD.OpenFile()); // Ouvrir le flux en lecture

                    while ((ligne = SR.ReadLine()) != null) //lire le fichier ligne par ligne 
                    {
                        lbxChamps.Items.Add(ligne);//Permet de remplire la listbox 
                    }
                    SR.Close(); // fermer le flux

                }
                catch
                {
                    MessageBox.Show("La table n'a pas réussi à être importer");
                }
            }
        }

        private void convert_Click(object sender, EventArgs e)
        {
            string myFile = Path.GetFileNameWithoutExtension(OFD.ToString());//Permet de récuperer le nom du fichier
            myFile = myFile.ToUpper();//Permet de mettre le nom du fichier en majuscule
            SFD.Title = "Les fichiers texte."; //titre de la boite de dialogue
            SFD.Filter = "Fichiers texte|*.sql"; //Enregistre les fichiers en .sql
            SFD.FileName = myFile;//Nomme le fichier par le nom du fichier convertit.
            if (SFD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {        
                    StreamWriter sr = new StreamWriter(SFD.FileName);//Permet d'écrire dans le fichier créé
                    lbxChamps.SetSelected(1, true);
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sr.Write("INSERT INTO " + myFile + "(");//écrit le début de la requête
                    sb.Append(lbxChamps.Items[0].ToString());
                    //Permet d'inserer le nom des champs
                    insertTab = sb.ToString().Split(';');
                    for (int i = 0; i < insertTab.Length; i++)
                    {
                        if (i != insertTab.Length - 1)
                        {
                            sr.Write(insertTab[i] + ",");
                        }
                        else
                        {
                            sr.Write(insertTab[i]);
                        }
                    }
                    sb.Clear();
                    sr.WriteLine(")");

                    //Permet d'inserer les valeurs:
                    sr.WriteLine("VALUES");
                    lbxChamps.Items.Remove(lbxChamps.Items[0]);//Enlever la première ligne du fichier pour éviter d'avoir dans values le nom des champs
                    int count = 0;
                    foreach (object item in lbxChamps.Items)
                    {                    
                        sr.Write("('");
                        sb.Append(item.ToString());
                        valuesTable = sb.ToString().Split(';');
                        for (int i =0; i < valuesTable.Length; i++)
                        {
                            if (i != valuesTable.Length - 1)
                            {
                                sr.Write(valuesTable[i] + "','");
                            }
                            else
                            {
                                sr.Write(valuesTable[i] + "'");
                            }
                            
                        }
                        count++;
                        if (count != lbxChamps.Items.Count)
                        {
                            sr.WriteLine("),");
                        }
                        else
                        {
                            sr.WriteLine(");");
                        }
                        sb.Clear();
                    }
                    sr.Close();
                }
                catch
                {
                    MessageBox.Show("Erreur");
                }
            }
        }

        private void bntDossier_Click(object sender, EventArgs e)
        {
            //Permet d'afficher le contenu d'un dossier dans lbxDossier
            if (FDB.ShowDialog() == DialogResult.OK)
            {
                lbxDossier.Text = FDB.SelectedPath;

                DirectoryInfo dir = new DirectoryInfo(FDB.SelectedPath);
                name = FDB.SelectedPath + "/";
                FileInfo[] fichiers = dir.GetFiles();

                lbxDossier.Items.Clear();

                foreach (FileInfo fichier in fichiers)
                {
                    lbxDossier.Items.Add(fichier.Name);
                }

            }
        }

        private void lbxDossier_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Permet d'afficher dans dans lbxChamp
            nom = Convert.ToString(lbxDossier.SelectedItem);
            String ligne;

            try
            {
                lbxChamps.Items.Clear();
                StreamReader SR = new StreamReader(name + nom);
                while ((ligne = SR.ReadLine()) != null)
                {

                    lbxChamps.Items.Add(ligne);
                }
                SR.Close();
            }
            catch
            {
                MessageBox.Show("Erreur le contenu du dossier n'a pas réussi à être afficher.");
            }    
        }
    }
}
    

