-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 31 jan. 2023 à 16:06
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `arbitre`
--

DROP TABLE IF EXISTS `arbitre`;
CREATE TABLE IF NOT EXISTS `arbitre` (
  `CodeA` varchar(4) NOT NULL,
  `NomA` varchar(20) DEFAULT NULL,
  `PrenomA` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CodeA`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `arbitre`
--

INSERT INTO `arbitre` (`CodeA`, `NomA`, `PrenomA`) VALUES
('A1', 'Lefort', 'Philippe'),
('A2', 'Trébeau', 'Sébastien'),
('A3', 'Ritterlan', 'Hervé'),
('A4', 'Rubens', 'Stéphane'),
('A5', 'Leoperon', 'Philippe'),
('A6', 'Durand', 'Jean Charles'),
('A7', 'Stéfanin', 'Loïc'),
('A8', 'Renom', 'Pierre'),
('A9', 'Fieffe', 'Eric'),
('A10', 'Lepont', 'Jacques'),
('A11', 'Pierrin', 'Marcel'),
('A12', 'Monin', 'Jean');

-- --------------------------------------------------------

--
-- Structure de la table `arbitrer`
--

DROP TABLE IF EXISTS `arbitrer`;
CREATE TABLE IF NOT EXISTS `arbitrer` (
  `CodeArbitre` varchar(4) NOT NULL,
  `CodeMatch` varchar(4) NOT NULL,
  `RoleArbitre` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CodeArbitre`,`CodeMatch`),
  KEY `CodeMatch` (`CodeMatch`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `arbitrer`
--

INSERT INTO `arbitrer` (`CodeArbitre`, `CodeMatch`, `RoleArbitre`) VALUES
('A1', 'M1', 'terrain'),
('A12', 'M1', 'table'),
('A10', 'M10', 'terrain'),
('A4', 'M10', 'table'),
('A11', 'M11', 'terrain'),
('A5', 'M11', 'table'),
('A12', 'M12', 'terrain'),
('A6', 'M12', 'table'),
('A1', 'M13', 'terrain'),
('A5', 'M13', 'table'),
('A3', 'M14', 'terrain'),
('A7', 'M14', 'table'),
('A6', 'M15', 'terrain'),
('A8', 'M15', 'table'),
('A9', 'M16', 'terrain'),
('A11', 'M16', 'table'),
('A10', 'M17', 'terrain'),
('A12', 'M17', 'table'),
('A2', 'M18', 'terrain'),
('A4', 'M18', 'table'),
('A1', 'M19', 'terrain'),
('A10', 'M19', 'table'),
('A11', 'M2', 'terrain'),
('A4', 'M2', 'table'),
('A2', 'M20', 'terrain'),
('A12', 'M20', 'table'),
('A3', 'M21', 'terrain'),
('A11', 'M21', 'table'),
('A4', 'M22', 'terrain'),
('A9', 'M22', 'table'),
('A5', 'M23', 'terrain'),
('A8', 'M23', 'table'),
('A6', 'M24', 'terrain'),
('A7', 'M24', 'table'),
('A10', 'M3', 'terrain'),
('A2', 'M3', 'table'),
('A5', 'M4', 'terrain'),
('A9', 'M4', 'table'),
('A3', 'M5', 'terrain'),
('A8', 'M5', 'table'),
('A6', 'M6', 'terrain'),
('A7', 'M6', 'table'),
('A2', 'M7', 'terrain'),
('A7', 'M7', 'table'),
('A2', 'M8', 'terrain'),
('A8', 'M8', 'table'),
('A3', 'M9', 'terrain'),
('A9', 'M9', 'table');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `CodeCat` varchar(4) NOT NULL,
  `LibelleC` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CodeCat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`CodeCat`, `LibelleC`) VALUES
('S1', 'Junior'),
('S2', 'Senior');

-- --------------------------------------------------------

--
-- Structure de la table `club`
--

DROP TABLE IF EXISTS `club`;
CREATE TABLE IF NOT EXISTS `club` (
  `CodeC` varchar(4) NOT NULL,
  `NomC` varchar(20) DEFAULT NULL,
  `AdresseC` varchar(30) DEFAULT NULL,
  `VilleC` varchar(30) DEFAULT NULL,
  `CPC` varchar(5) DEFAULT NULL,
  `TelC` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`CodeC`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `club`
--

INSERT INTO `club` (`CodeC`, `NomC`, `AdresseC`, `VilleC`, `CPC`, `TelC`) VALUES
('C1', 'SF Belleville', '', 'Verdun', '55100', ''),
('C2', 'SCA Epinal', '', 'Epinal', '88000', ''),
('C3', 'USL Mineenne', '', 'St Mihiel', '55300', ''),
('C4', 'FC Toul', '', 'Toul', '54200', ''),
('C5', 'US Etain', '', 'Etain', '55400', ''),
('C6', 'ESP Lunéville', '', 'Lunéville', '54300', ''),
('C7', 'FC Velaines', '', 'Velaines', '55500', ''),
('C8', 'FCSTD', '', 'Saint Dié', '88100', ''),
('C9', 'District Vosges', '', 'Epinal', '88000', ''),
('C10', 'FC Val Dunois', '', 'Behonne', '55000', ''),
('C11', 'FC Bruch Forbach', '', 'Forbach', '76000', ''),
('C12', 'FC Commercy', '', 'Velaines', '55500', '');

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
CREATE TABLE IF NOT EXISTS `equipe` (
  `CodeE` varchar(4) NOT NULL,
  `CodeClub` varchar(4) DEFAULT NULL,
  `CodeCateg` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`CodeE`),
  KEY `CodeClub` (`CodeClub`),
  KEY `CodeCateg` (`CodeCateg`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `equipe`
--

INSERT INTO `equipe` (`CodeE`, `CodeClub`, `CodeCateg`) VALUES
('E1', 'C1', 'S1'),
('E2', 'C1', 'S1'),
('E3', 'C2', 'S1'),
('E4', 'C3', 'S1'),
('E5', 'C4', 'S1'),
('E6', 'C3', 'S1'),
('E7', 'C2', 'S1'),
('E8', 'C5', 'S1'),
('E9', 'C6', 'S1'),
('E10', 'C4', 'S1'),
('E11', 'C6', 'S1'),
('E12', 'C5', 'S1'),
('E13', 'C5', 'S2'),
('E14', 'C6', 'S2'),
('E15', 'C5', 'S2'),
('E16', 'C4', 'S2'),
('E17', 'C2', 'S2'),
('E18', 'C1', 'S2'),
('E19', 'C6', 'S2'),
('E20', 'C2', 'S2'),
('E21', 'C1', 'S2'),
('E22', 'C4', 'S2'),
('E23', 'C3', 'S2'),
('E24', 'C3', 'S2');

-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--

DROP TABLE IF EXISTS `joueur`;
CREATE TABLE IF NOT EXISTS `joueur` (
  `NumJ` int(11) NOT NULL,
  `NomJ` varchar(20) DEFAULT NULL,
  `PrenomJ` varchar(20) DEFAULT NULL,
  `DateNaissJ` date DEFAULT NULL,
  `NumLicJ` varchar(10) DEFAULT NULL,
  `CodeEquipe` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`NumJ`),
  KEY `CodeEquipe` (`CodeEquipe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `joueur`
--

INSERT INTO `joueur` (`NumJ`, `NomJ`, `PrenomJ`, `DateNaissJ`, `NumLicJ`, `CodeEquipe`) VALUES
(1, 'ACHARD', 'Patrick', '0000-00-00', '8752504422', 'E11'),
(2, 'ANDRE', 'Pascal', '0000-00-00', '5008121528', 'E11'),
(3, 'ANDRI', 'Henri', '0000-00-00', '7629104910', 'E16'),
(4, 'ARNAUD', 'Philippe', '0000-00-00', '2439258097', 'E5'),
(5, 'AUDOUARD', 'Jean-Claude', '0000-00-00', '6281306978', 'E4'),
(6, 'AUGAGNEUR', 'Jean-Paul', '0000-00-00', '1599486091', 'E21'),
(7, 'AUROI', 'Lucien', '0000-00-00', '1717995615', 'E23'),
(8, 'BALEYDIER', 'Henri', '0000-00-00', '2380284764', 'E9'),
(9, 'BALLIGAND', 'Serge', '0000-00-00', '1612514095', 'E8'),
(10, 'BAVUSO', 'François', '0000-00-00', '5000559614', 'E5'),
(11, 'BAYET', 'Benoit', '0000-00-00', '4589534716', 'E24'),
(12, 'BAYET', 'Bruno', '0000-00-00', '6740352810', 'E14'),
(13, 'BAYLE', 'Alexis', '0000-00-00', '4351189386', 'E7'),
(14, 'BENOIT', 'Lucien', '0000-00-00', '4632074453', 'E4'),
(15, 'BERGER', 'Christian', '0000-00-00', '9296088373', 'E11'),
(16, 'BERNARD', 'Alain', '0000-00-00', '8414640626', 'E11'),
(17, 'BERTHELIER', 'Patrick', '0000-00-00', '1479145860', 'E16'),
(18, 'BERTHOLON', 'Gérard', '0000-00-00', '3485242400', 'E16'),
(19, 'BLANCHIN', 'Gérard', '0000-00-00', '8969623885', 'E6'),
(20, 'BONNAMOUR', 'Etienne', '0000-00-00', '2438961857', 'E13'),
(21, 'BORY', 'Philippe', '0000-00-00', '9203553597', 'E8'),
(22, 'BOS', 'Stanley', '0000-00-00', '4751805248', 'E1'),
(23, 'BOUCHET', 'Philippe', '0000-00-00', '5530430833', 'E11'),
(24, 'BOUCHET', 'Matéo', '0000-00-00', '2639252741', 'E7'),
(25, 'BOURDON', 'Alain', '0000-00-00', '9193417985', 'E22'),
(26, 'BOURDON', 'Denis', '0000-00-00', '1089306218', 'E4'),
(27, 'BOYER', 'Christian', '0000-00-00', '7012541756', 'E1'),
(28, 'BRISSE', 'Thierry', '0000-00-00', '5619171786', 'E3'),
(29, 'BROCHIER', 'Alain', '0000-00-00', '5210040608', 'E9'),
(30, 'BROERS', 'Detlef', '0000-00-00', '1263801968', 'E3'),
(31, 'CARNAT', 'Robert', '0000-00-00', '5605470797', 'E24'),
(32, 'CHAMBON', 'Pierre', '0000-00-00', '2726698993', 'E11'),
(33, 'CHAPON', 'André', '0000-00-00', '1823471429', 'E6'),
(34, 'CHAUSSAT', 'Jean-Marc', '0000-00-00', '7277253144', 'E8'),
(35, 'CHERPIN', 'Guy', '0000-00-00', '5062487255', 'E16'),
(36, 'CHIL', 'Gérard', '0000-00-00', '5398813190', 'E18'),
(37, 'CIVRAIS', 'Christian', '0000-00-00', '9512374929', 'E11'),
(38, 'CLAVELLOUX', 'Julien', '0000-00-00', '1416905043', 'E22'),
(39, 'CLUZEL', 'Sylvain', '0000-00-00', '9660304226', 'E6'),
(40, 'COMBETTE', 'Yves', '0000-00-00', '6254055335', 'E23'),
(41, 'COQUARD', 'Alexandre', '0000-00-00', '8503836205', 'E3'),
(42, 'CORNET', 'Antoine', '0000-00-00', '9648829727', 'E24'),
(43, 'COTTE', 'Jean-René', '0000-00-00', '5110136938', 'E8'),
(44, 'COUCHAUD', 'Gérard', '0000-00-00', '5112015742', 'E11'),
(45, 'CROS', 'Aurélien', '0000-00-00', '6869070924', 'E4'),
(46, 'DEBOVE', 'Christian', '0000-00-00', '2145441712', 'E9'),
(47, 'DECLINE', 'Robert', '0000-00-00', '7551189660', 'E24'),
(48, 'DEFOSSE', 'Michel', '0000-00-00', '2352941467', 'E3'),
(49, 'DELORME', 'Joseph', '0000-00-00', '9662425113', 'E22'),
(50, 'DELORME', 'André', '0000-00-00', '5295089264', 'E12'),
(51, 'DENIS', 'Clément', '0000-00-00', '9329920308', 'E18'),
(52, 'DESLANDES', 'Pascal', '0000-00-00', '6866170500', 'E15'),
(53, 'DESSERLE', 'Guy', '0000-00-00', '3067332808', 'E2'),
(54, 'DESSERT', 'Stéphane', '0000-00-00', '7174232436', 'E3'),
(55, 'DIXNEUF', 'Patrick', '0000-00-00', '577468970', 'E16'),
(56, 'DUBUS', 'Yvon', '0000-00-00', '9941118082', 'E11'),
(57, 'DUPIN', 'Roger', '0000-00-00', '9167728364', 'E1'),
(58, 'EMONET', 'Henri', '0000-00-00', '7662022926', 'E8'),
(59, 'EPINAT', 'Olivier', '0000-00-00', '9317710292', 'E17'),
(60, 'ESCOFFIER', 'Christian', '0000-00-00', '8424670982', 'E18'),
(61, 'FAIDIT', 'Serge', '0000-00-00', '7440152704', 'E18'),
(62, 'FAYOLLE', 'Pierre', '0000-00-00', '2538945178', 'E1'),
(63, 'FEOLA', 'Jean-Michel', '0000-00-00', '9979318508', 'E4'),
(64, 'FERRATON', 'Michel', '0000-00-00', '9569690997', 'E9'),
(65, 'FERRE', 'Jean-Paul', '0000-00-00', '6313720571', 'E11'),
(66, 'FERREOL', 'Emile', '0000-00-00', '6013397169', 'E3'),
(67, 'FEUILLASTRE', 'Alain', '0000-00-00', '4268372373', 'E19'),
(68, 'FORISSIER', 'Louis', '0000-00-00', '2516505686', 'E2'),
(69, 'FORISSIER', 'Pierrick', '0000-00-00', '7665828677', 'E16'),
(70, 'FRAGNE', 'Marcel', '0000-00-00', '6695628907', 'E2'),
(71, 'FRANCE', 'Fernand', '0000-00-00', '3360603300', 'E13'),
(72, 'GENEBRIER', 'Gaspard', '0000-00-00', '7032149395', 'E7'),
(73, 'GENEVRIER', 'Gérard', '0000-00-00', '4265739911', 'E3'),
(74, 'GIROMINI', 'Guy', '0000-00-00', '5813407382', 'E13'),
(75, 'GRENETIER', 'Etienne', '0000-00-00', '1566199758', 'E19'),
(76, 'GRIOT', 'Gilbert', '0000-00-00', '9279939640', 'E10'),
(77, 'GUILLAUMONT', 'Alain', '0000-00-00', '6390936333', 'E21'),
(78, 'GUILLOT', 'Claude', '0000-00-00', '7374530580', 'E14'),
(79, 'GUILLOT', 'André', '0000-00-00', '369977694', 'E4'),
(80, 'GUILLOT', 'Pierre', '0000-00-00', '3201489381', 'E12'),
(81, 'GUILLOT', 'Jean', '0000-00-00', '1380121602', 'E7'),
(82, 'HOGG', 'Jean-Claude', '0000-00-00', '2498442285', 'E23'),
(83, 'HUBERT', 'André', '0000-00-00', '2667700509', 'E12'),
(84, 'JACQUEMOND', 'Olivier', '0000-00-00', '8343781406', 'E5'),
(85, 'JAUMOTTE', 'Jacques', '0000-00-00', '2190873432', 'E3'),
(86, 'KLEIN', 'Jean-Luc', '0000-00-00', '6959171809', 'E15'),
(87, 'KURADJIAN', 'Alain', '0000-00-00', '8202240814', 'E16'),
(88, 'LACAZE', 'Roger', '0000-00-00', '7751213624', 'E9'),
(89, 'LARDEREAU', 'Robert', '0000-00-00', '6150051573', 'E16'),
(90, 'LAUROT', 'Pascale', '0000-00-00', '5896614054', 'E4'),
(91, 'LAVAUD', 'Jacques', '0000-00-00', '9113309124', 'E11'),
(92, 'LECONNETABLE', 'Claude', '0000-00-00', '7501456408', 'E7'),
(93, 'LELOUP', 'Alain', '0000-00-00', '6666653550', 'E20'),
(94, 'LEPETIT', 'François', '0000-00-00', '7744550552', 'E15'),
(95, 'LEROUX', 'Philippe', '0000-00-00', '8945439794', 'E4'),
(96, 'LORON', 'André', '0000-00-00', '1487102996', 'E8'),
(97, 'LORON', 'Louis', '0000-00-00', '2594917897', 'E10'),
(98, 'MANCINI', 'Olivier', '0000-00-00', '1528959838', 'E2'),
(99, 'MARECHAL', 'Morgan', '0000-00-00', '7177573713', 'E21'),
(100, 'MARECHAL', 'Jacques', '0000-00-00', '9267034117', 'E10'),
(101, 'MASSELIN', 'André', '0000-00-00', '5390504457', 'E6'),
(102, 'MASSON', 'Joël', '0000-00-00', '8899736825', 'E17'),
(103, 'MEJEAN', 'Jacques', '0000-00-00', '1208063991', 'E8'),
(104, 'MIGRAINE', 'Jacques', '0000-00-00', '3124650721', 'E20'),
(105, 'MOLLON', 'Jean', '0000-00-00', '4405443049', 'E22'),
(106, 'MONIER', 'Bernard', '0000-00-00', '8012523891', 'E3'),
(107, 'MORLIN', 'Guy', '0000-00-00', '9122811682', 'E7'),
(108, 'MOUCHARAT', 'Jean-Claude', '0000-00-00', '6059965993', 'E3'),
(109, 'MOULARD', 'André', '0000-00-00', '9953077256', 'E3'),
(110, 'MYARD', 'André', '0000-00-00', '4636027059', 'E12'),
(111, 'NAACKE', 'Philippe', '0000-00-00', '9832311817', 'E3'),
(112, 'NOURRISSON', 'Brigitte', '0000-00-00', '1930194461', 'E4'),
(113, 'OLLIVIER', 'Franck', '0000-00-00', '7107684996', 'E5'),
(114, 'PAGNAN', 'Albert', '0000-00-00', '2706294236', 'E6'),
(115, 'PARANT', 'Daniel', '0000-00-00', '7904418855', 'E19'),
(116, 'PARRA', 'Michel', '0000-00-00', '3145994735', 'E20'),
(117, 'PASCA', 'Jean', '0000-00-00', '5247926734', 'E20'),
(118, 'PAULIGNAN', 'Martine', '0000-00-00', '5240138538', 'E22'),
(119, 'PEDARRE', 'Michel', '0000-00-00', '8412537400', 'E12'),
(120, 'PEREZ', 'Martin', '0000-00-00', '5441630538', 'E15'),
(121, 'PERRET', 'Olivier', '0000-00-00', '5549494314', 'E7'),
(122, 'PERRET', 'Richard', '0000-00-00', '9160541820', 'E22'),
(123, 'PETIT', 'Nicolas', '0000-00-00', '4427313422', 'E17'),
(124, 'PEYRARD', 'Yves', '0000-00-00', '5334170881', 'E17'),
(125, 'PHILIBERT', 'Georges', '0000-00-00', '6624998759', 'E10'),
(126, 'PIALLAT', 'René', '0000-00-00', '7035004250', 'E24'),
(127, 'PLANCHET', 'Marcel', '0000-00-00', '8901300065', 'E15'),
(128, 'POIROT', 'Georges', '0000-00-00', '5320861385', 'E19'),
(129, 'POULALIER', 'Paul', '0000-00-00', '1477564890', 'E19'),
(130, 'POYET', 'Noel', '0000-00-00', '6480900902', 'E23'),
(131, 'PREBET', 'Danielle', '0000-00-00', '6283891598', 'E5'),
(132, 'PREBET', 'Gérard', '0000-00-00', '4753918268', 'E6'),
(133, 'PREYNAT', 'Jean louis', '0000-00-00', '9115687879', 'E24'),
(134, 'PREYNAT', 'Maurice', '0000-00-00', '3785832762', 'E10'),
(135, 'RABEYRIN', 'Richard', '0000-00-00', '8301013634', 'E1'),
(136, 'RANCHON', 'Bernard', '0000-00-00', '6287320605', 'E23'),
(137, 'RANCHON', 'Jean-François', '0000-00-00', '4837398970', 'E2'),
(138, 'RASCLE', 'Pierre', '0000-00-00', '6487555509', 'E6'),
(139, 'RAYMOND', 'Serge', '0000-00-00', '6056133143', 'E6'),
(140, 'REMILLIEUX', 'Jacques', '0000-00-00', '3676388692', 'E11'),
(141, 'RENAUD', 'Jacques', '0000-00-00', '2058192146', 'E3'),
(142, 'RICCOBENE', 'Maxime', '0000-00-00', '8038967047', 'E2'),
(143, 'RIFELDE', 'Robert', '0000-00-00', '4578612266', 'E11'),
(144, 'ROBERT', 'Denis', '0000-00-00', '4602963164', 'E5'),
(145, 'ROLHION', 'Daniel', '0000-00-00', '4816300240', 'E7'),
(146, 'SAVOLDELLI', 'René', '0000-00-00', '5471537711', 'E4'),
(147, 'SCHREIBER', 'Robert', '0000-00-00', '8567175752', 'E19'),
(148, 'SCILY', 'Gérard', '0000-00-00', '5897253946', 'E17'),
(149, 'SELLIER', 'Laurent', '0000-00-00', '3696525495', 'E23'),
(150, 'SOLA', 'Albert', '0000-00-00', '4584663668', 'E10'),
(151, 'SOULAS', 'Gérard', '0000-00-00', '5621270166', 'E12'),
(152, 'SOULIER', 'Daniel', '0000-00-00', '9988395165', 'E15'),
(153, 'SUCHAUT', 'Antoine', '0000-00-00', '5516956642', 'E9'),
(154, 'SUCHAUT', 'Thierry', '0000-00-00', '4916004100', 'E4'),
(155, 'TABART', 'Gabriel', '0000-00-00', '4089496660', 'E1'),
(156, 'TEYSSIER', 'Serge', '0000-00-00', '6406878418', 'E3'),
(157, 'TISSOT', 'Roger', '0000-00-00', '5757981544', 'E14'),
(158, 'TIXIER', 'Georges', '0000-00-00', '7143492558', 'E18'),
(159, 'TRAN', 'Charly', '0000-00-00', '8753149059', 'E2'),
(160, 'TRAN', 'Auguste', '0000-00-00', '6392991940', 'E7'),
(161, 'TRIOMPHE', 'Jérémy', '0000-00-00', '9472042185', 'E17'),
(162, 'VALLAT', 'Daniel', '0000-00-00', '7085153078', 'E5'),
(163, 'VAUX', 'Roland', '0000-00-00', '9788953727', 'E20'),
(164, 'VILLARD', 'Marc', '0000-00-00', '2707953508', 'E6'),
(165, 'VIVIAND', 'Louis', '0000-00-00', '1131332589', 'E4'),
(166, 'WISNIEWSKI', 'Romain', '0000-00-00', '9185944706', 'E8'),
(167, 'ZAPOTOCKY', 'Jean', '0000-00-00', '5333775982', 'E2'),
(168, 'ZUDDAS', 'Pierre', '0000-00-00', '3931918364', 'E4');

-- --------------------------------------------------------

--
-- Structure de la table `matche`
--

DROP TABLE IF EXISTS `matche`;
CREATE TABLE IF NOT EXISTS `matche` (
  `CodeM` varchar(4) NOT NULL,
  `DateM` date DEFAULT NULL,
  `LieuM` int(11) DEFAULT NULL,
  `ScoreEquipe1` int(11) DEFAULT NULL,
  `ScoreEquipe2` int(11) DEFAULT NULL,
  `CodeEquipe1` varchar(4) DEFAULT NULL,
  `CodeEquipe2` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`CodeM`),
  KEY `CodeEquipe1` (`CodeEquipe1`),
  KEY `CodeEquipe2` (`CodeEquipe2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matche`
--

INSERT INTO `matche` (`CodeM`, `DateM`, `LieuM`, `ScoreEquipe1`, `ScoreEquipe2`, `CodeEquipe1`, `CodeEquipe2`) VALUES
('M1', '0000-00-00', 0, 1, 3, 'E1', 'E2'),
('M2', '0000-00-00', 0, 2, 2, 'E5', 'E4'),
('M3', '0000-00-00', 0, 1, 4, 'E3', 'E12'),
('M4', '0000-00-00', 0, 2, 2, 'E8', 'E7'),
('M5', '0000-00-00', 0, 3, 3, 'E6', 'E10'),
('M6', '0000-00-00', 0, 2, 4, 'E9', 'E11'),
('M7', '0000-00-00', 0, 1, 3, 'E4', 'E1'),
('M8', '0000-00-00', 0, 0, 2, 'E12', 'E3'),
('M9', '0000-00-00', 0, 1, 0, 'E7', 'E6'),
('M10', '0000-00-00', 0, 3, 1, 'E2', 'E8'),
('M11', '0000-00-00', 0, 2, 0, 'E5', 'E9'),
('M12', '0000-00-00', 0, 2, 2, 'E11', 'E10'),
('M13', '0000-00-00', 0, 0, 0, 'E23', 'E12'),
('M14', '0000-00-00', 0, 1, 3, 'E20', 'E13'),
('M15', '0000-00-00', 0, 3, 4, 'E22', 'E14'),
('M16', '0000-00-00', 0, 2, 3, 'E19', 'E16'),
('M17', '0000-00-00', 0, 3, 1, 'E21', 'E17'),
('M18', '0000-00-00', 0, 0, 1, 'E15', 'E18'),
('M19', '0000-00-00', 0, 1, 2, 'E15', 'E19'),
('M20', '0000-00-00', 0, 2, 1, 'E14', 'E20'),
('M21', '0000-00-00', 0, 4, 3, 'E16', 'E21'),
('M22', '0000-00-00', 0, 2, 2, 'E18', 'E22'),
('M23', '0000-00-00', 0, 3, 1, 'E17', 'E23'),
('M24', '0000-00-00', 0, 0, 0, 'E24', 'E13');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
